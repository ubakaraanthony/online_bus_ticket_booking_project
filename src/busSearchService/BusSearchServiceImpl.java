package busSearchService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import ActionBean.BookedSeatsBean;
import ActionBean.SingleBusBean;
import Config.DBConfiguration;
import beanclass.DistrictBeanClass;

public class BusSearchServiceImpl extends DBConfiguration implements BusSearchService {

	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	DistrictBeanClass busList;
	@Override
	public List<DistrictBeanClass> SearchBusses(String from, String to) {
		try {
			Class.forName(driver_class);
			con = DriverManager.getConnection(db_url, user, pass);
			System.out.println("DB Connected");
			ps = con.prepareStatement("Select * from bus_details where from_place=? && to_place=?");
			ps.setString(1, from);
			ps.setString(2, to);
			rs = ps.executeQuery();
			List<DistrictBeanClass> busLi = new ArrayList<>();
			while(rs.next()) {
				busList=new DistrictBeanClass();
				busList.setId(rs.getString("bus_id"));
				busList.setBusName(rs.getString("bus_name"));
				busList.setBusType(rs.getString("bus_type"));
				busList.setFrom(rs.getString("from_place"));
				busList.setDepaturePlace(rs.getString("depature_place"));
				busList.setStartTime(rs.getString("start_time"));
				busList.setTo(rs.getString("to_place"));
				busList.setArrivalPlace(rs.getString("arrival_place"));
				busList.setAmount(rs.getString("amount"));
				busList.setAvailableSeats(String.valueOf(rs.getString("available_seats")));
				busLi.add(busList);
			}
			return busLi;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public SingleBusBean getBusById(String id) {
		SingleBusBean singleBusBean = SingleBusBean.getInstance();
		try {
			Class.forName(driver_class);
			con = DriverManager.getConnection(db_url, user, pass);
			System.out.println("DB Connected");
			ps = con.prepareStatement("Select * from bus_details where bus_id=?"); 
			ps.setString(1, id);
			rs = ps.executeQuery();

			if(rs.next()) {
				System.out.println("Data Existed");
				singleBusBean.setId(rs.getString("bus_id"));
				singleBusBean.setBusName(rs.getString("bus_name"));
				singleBusBean.setBusType(rs.getString("bus_type"));
				singleBusBean.setFrom(rs.getString("from_place"));
				singleBusBean.setDepaturePlace(rs.getString("depature_place"));
				singleBusBean.setStartTime(rs.getString("start_time"));
				singleBusBean.setTo(rs.getString("to_place"));
				singleBusBean.setArrivalPlace(rs.getString("arrival_place"));
				singleBusBean.setAmount(rs.getString("amount"));
				singleBusBean.setAvailableSeats(String.valueOf(rs.getString("available_seats")));

				ps = con.prepareStatement("Select booked_seat_no from booked_seats where bus_id=?"); 
				ps.setString(1, id);
				rs = ps.executeQuery();
				List<String> seats = new ArrayList<String>();
				while(rs.next()) {
					seats.add(rs.getString("booked_seat_no"));
				}
				if(!seats.isEmpty()) {
					System.out.println("Seats added");
					singleBusBean.setBookedSeats(seats);
				}
				System.out.println("Testing"+ singleBusBean.toString());
				return singleBusBean;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
