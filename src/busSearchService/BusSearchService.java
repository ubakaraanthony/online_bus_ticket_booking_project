package busSearchService;

import java.util.List;

import ActionBean.SingleBusBean;
import beanclass.DistrictBeanClass;

public interface BusSearchService {
	List<DistrictBeanClass> SearchBusses(String from, String to);
	SingleBusBean getBusById(String id);
}
