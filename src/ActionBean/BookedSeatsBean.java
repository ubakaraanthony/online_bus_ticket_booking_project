package ActionBean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name="booked")
@RequestScoped
public class BookedSeatsBean {
	private List<String> bookedSeats;
	private static BookedSeatsBean bookedSeatsBean = new BookedSeatsBean();
	
	private BookedSeatsBean() {
		
	}
	public static BookedSeatsBean getObject() {
		return bookedSeatsBean;
	}

	public List<String> getBookedSeats() {
		return bookedSeats;
	}

	public void setBookedSeats(List<String> bookedSeats) {
		this.bookedSeats = bookedSeats;
	}
}
