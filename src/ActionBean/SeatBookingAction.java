package ActionBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import SeatBooking.SeatBookingService;
import SeatBooking.SeatBookingServiceImpl;

@ManagedBean(name="seatBooking")
@RequestScoped
public class SeatBookingAction {
	public String bookSeat(SeatsBean seatsBean,PaymentOptions paymentOption) {
		System.out.println(String.join(",", seatsBean.getSeats()));
		String seats = String.join(",", seatsBean.getSeats());
		UserBeanClass userBeanClass = (UserBeanClass) FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("userBeanClass");
		System.out.println("user Name: "+userBeanClass.getUser_name());
		String uname = userBeanClass.getUser_name();
		SingleBusBean singleBusBean = SingleBusBean.getInstance();
		String busId = singleBusBean.getId();
		int amount = Integer.parseInt(singleBusBean.getAmount());
		
		int seatsCount = seats.split(",").length ;
		int paidAmount = amount * seatsCount;
		paymentOption.setPaidAmount(String.valueOf(paidAmount));
		SeatBookingService seatBookingService = new SeatBookingServiceImpl();
		boolean status = seatBookingService.bookSeats(seats,busId,uname);
		System.out.println("Status: "+status);
		if(status) {
			paymentOption.setBusId(busId);
			paymentOption.setUname(uname);
			boolean payOption = seatBookingService.payOption(paymentOption);
			if(payOption) {
				System.out.println("MyTrips invoked");
				return "/jsp/MyTrips.xhtml";
			}
		}
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("err", "Something Went Wrong");
		System.out.println("seatBooking invoked");
		return "faces/jsp/SeatBooking.jsp";
	}
}
