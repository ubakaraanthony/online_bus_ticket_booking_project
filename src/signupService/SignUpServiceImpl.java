package signupService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import Config.DBConfiguration;
import beanclass.SignUpBeanClass;

public class SignUpServiceImpl extends DBConfiguration implements SignUpService {
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	@Override
	public boolean signUpUser(SignUpBeanClass signUpBeanClass) {
		System.out.println(signUpBeanClass.getUserName());
		System.out.println(signUpBeanClass.getPassword());
		System.out.println(signUpBeanClass.getFirstName());
		System.out.println(signUpBeanClass.getLastName());
		System.out.println(signUpBeanClass.getContact());
		System.out.println(signUpBeanClass.getEmail());
		try {
			Class.forName(driver_class);
			con = DriverManager.getConnection(db_url, user, pass);
			System.out.println("DB Connected");
			ps = con.prepareStatement("select max(id) from user_info");
			rs = ps.executeQuery();
			int row = 0;
			int num=0;
			if(rs.next()) {
			row = rs.getInt(1);
			}
			System.out.println(row+1);
			ps = con.prepareStatement("insert into user_info (id,user_name,password,fname,lname,contact_number,email) values(?,?,?,?,?,?,?)");
			ps.setInt(1, row+1);
			ps.setString(2, signUpBeanClass.getUserName());
			ps.setString(3, signUpBeanClass.getPassword());
			ps.setString(4, signUpBeanClass.getFirstName());
			ps.setString(5, signUpBeanClass.getLastName());
			ps.setString(6, signUpBeanClass.getContact());
			ps.setString(7, signUpBeanClass.getEmail());
			num = ps.executeUpdate();
			if(num==1) {
				return true;
			}
		}	
		catch(Exception e) {
			e.printStackTrace();
		}
		return false;
		
}
}