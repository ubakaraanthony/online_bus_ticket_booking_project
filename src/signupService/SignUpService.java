package signupService;

import beanclass.SignUpBeanClass;

public interface SignUpService {
	public boolean signUpUser(SignUpBeanClass signUpBeanClass);
}
