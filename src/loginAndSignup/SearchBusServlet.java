package loginAndSignup;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beanclass.DistrictBeanClass;
import busSearchService.BusSearchService;
import busSearchService.BusSearchServiceImpl;

/**
 * Servlet implementation class SearchBusServlet
 */
@WebServlet("/SearchBusServlet")
public class SearchBusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		System.out.println("=================");
		String from = request.getParameter("from");
		String to = request.getParameter("to");
		//		System.out.println(from+" "+to);
		BusSearchService busses = new BusSearchServiceImpl();
		try {
			List<DistrictBeanClass> busList = busses.SearchBusses(from,to);
			if(busList!=null) {
				System.out.println("comming");
				System.out.println(busList);
				HttpSession sess = request.getSession();
				sess.setAttribute("busList", busList);
				request.setAttribute("from", from);
				request.setAttribute("to", to);
				
				String url ="jsp/Bus_list.jsp";
				request.getRequestDispatcher(url).forward(request, response);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

}
