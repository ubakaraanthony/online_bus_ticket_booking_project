package loginAndSignup;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beanclass.SignUpBeanClass;
import loginService.LoginService;
import loginService.LoginServiceImpl;
import signupService.SignUpService;
import signupService.SignUpServiceImpl;

/**
 * Servlet implementation class SignUpServlet
 */
@WebServlet("/SignUp")
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String userName = request.getParameter("username");
		String fname = request.getParameter("first_name");
		String lname = request.getParameter("last_name");
		String contact = request.getParameter("contact");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		SignUpBeanClass signUpBeanClass = new SignUpBeanClass.SignUpBeanClassBuilder()
				.setUserName(userName)
				.setFirstName(fname)
				.setLastName(lname)
				.setContact(contact)
				.setEmail(email)
				.setPassword(password)
				.build();
		LoginService login = new LoginServiceImpl();
		try {
			boolean exists = login.checkUserExists(userName, password);
			if(exists==true) {
				System.out.println("userName Exists!!!");
				response.sendRedirect("./jsp/LogIn.jsp");
			}
			else {
				SignUpService signUpService = new SignUpServiceImpl();
				boolean inserted = false;
				inserted = signUpService.signUpUser(signUpBeanClass);
				if(inserted==true) {
					System.out.println(inserted);
					response.sendRedirect("./jsp/Menu_bar.jsp");
				}
				else {
					System.out.println(inserted);
					response.sendRedirect("./jsp/LogIn.jsp");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
