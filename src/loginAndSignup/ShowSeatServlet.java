package loginAndSignup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ActionBean.BookedSeatsBean;
import ActionBean.SingleBusBean;
import beanclass.DistrictBeanClass;
import busSearchService.BusSearchService;
import busSearchService.BusSearchServiceImpl;

/**
 * Servlet implementation class ShowSeatServlet
 */
@WebServlet("/ShowSeatServlet")
public class ShowSeatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Show Seat servlet");
		String busId =request.getParameter("busId");
		System.out.println("id"+busId);
		BusSearchService busSearchService = new BusSearchServiceImpl();
		SingleBusBean busDetail = busSearchService.getBusById(busId);
		BookedSeatsBean bookedSeatsBean = BookedSeatsBean.getObject();
		System.out.println("Bus Detail in Show seat servlet: "+busDetail);
		List<String> seats = new ArrayList<>();
		String[] str = null;
		if(busDetail.getBookedSeats()!=null) {
			for(int i=0;i<busDetail.getBookedSeats().size();i++) {
				str = busDetail.getBookedSeats().get(i).split(",");
				System.out.println(Arrays.asList(str));
				seats.addAll(Arrays.asList(str));
				System.out.println("Seats array size: "+seats.size());
			}

			if(busDetail.getBookedSeats().size()>0) {
				System.out.println("Contains or not: "+seats.contains("A11"));
				bookedSeatsBean.setBookedSeats(seats);
			}
		}

		try {
			//			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("seats", seats);
			request.setAttribute("seats", seats);
			request.setAttribute("busDet", busDetail);
			String url = "faces/jsp/SeatBooking.jsp";
			request.getRequestDispatcher(url).forward(request, response);
		}
		catch(Exception e) {
			System.out.println(e);
		}


	}

}
