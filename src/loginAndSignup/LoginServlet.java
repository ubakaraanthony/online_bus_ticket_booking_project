package loginAndSignup;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import loginService.LoginService;
import loginService.LoginServiceImpl;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		boolean exists = false;
		String username = request.getParameter("uname");
		String password = request.getParameter("pass");
		LoginService login = new LoginServiceImpl();
		try {
			exists = login.checkUserExists(username, password);
			if(exists==true) {
				Cookie ck = new Cookie("userName", username);
				response.addCookie(ck);
				request.getRequestDispatcher("./jsp/Menu_bar.jsp").forward(request, response);
//				response.sendRedirect("./jsp/Menu_bar.jsp");
			}
			else {
				request.setAttribute("err", "Invalid User Name and Password");
				request.getRequestDispatcher("./jsp/LogIn.jsp").forward(request, response);
//				response.sendRedirect("./jsp/LogIn.jsp");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
