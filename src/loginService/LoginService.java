package loginService;

public interface LoginService {
	public boolean checkUserExists(String username, String password) throws Exception;
}
