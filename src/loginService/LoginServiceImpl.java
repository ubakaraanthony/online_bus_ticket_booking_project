package loginService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;

import ActionBean.UserBeanClass;
import Config.DBConfiguration;

public class LoginServiceImpl extends DBConfiguration implements LoginService {
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	public boolean checkUserExists(String username, String password) throws Exception {
		try {
//			UserBeanClass userBeanClass = UserBeanClass.getObject();
			UserBeanClass userBeanClass = (UserBeanClass) FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("UserBeanClass");
			userBeanClass = userBeanClass==null? new UserBeanClass():userBeanClass;
			Class.forName(driver_class);
			con = DriverManager.getConnection(db_url, user, pass);
			System.out.println("DB Connected");
			ps = con.prepareStatement("Select * from user_info where user_name=? && "
					+ "password=?");
			ps.setString(1, username);
			ps.setString(2, password);
			rs = ps.executeQuery();
			if(rs.next()) {
				userBeanClass.setUser_name(rs.getString("user_name"));
				userBeanClass.setFirst_name(rs.getString("fname"));
				userBeanClass.setLast_name(rs.getString("lname"));
				userBeanClass.setContact(rs.getString("contact_number"));
				userBeanClass.setEmail(rs.getString("email"));
				FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().put("userBeanClass",userBeanClass);
				return true;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}