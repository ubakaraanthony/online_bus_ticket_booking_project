package beanclass;

public class DistrictBeanClass {
	private String id, busName, busType, 
	from, depaturePlace, startTime, to, 
	arrivalPlace, amount, availableSeats;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBusName() {
		return busName;
	}

	public void setBusName(String busName) {
		this.busName = busName;
	}

	public String getBusType() {
		return busType;
	}

	public void setBusType(String busType) {
		this.busType = busType;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getDepaturePlace() {
		return depaturePlace;
	}

	public void setDepaturePlace(String depaturePlace) {
		this.depaturePlace = depaturePlace;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getArrivalPlace() {
		return arrivalPlace;
	}

	public void setArrivalPlace(String arrivalPlace) {
		this.arrivalPlace = arrivalPlace;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAvailableSeats() {
		return availableSeats;
	}

	public void setAvailableSeats(String availableSeats) {
		this.availableSeats = availableSeats;
	}

	@Override
	public String toString() {
		return "DistrictBeanClass [id=" + id + ", busName=" + busName + ", busType=" + busType + ", from=" + from
				+ ", depaturePlace=" + depaturePlace + ", startTime=" + startTime + ", to=" + to + ", arrivalPlace="
				+ arrivalPlace + ", amount=" + amount + ", availableSeats=" + availableSeats + "]";
	}
}
