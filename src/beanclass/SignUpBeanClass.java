package beanclass;

public class SignUpBeanClass {
	private String userName;
	private String firstName;
	private String lastName;
	private String contact;
	private String email;
	private String password;
	
	public static class SignUpBeanClassBuilder{
		
		private String userName;
		private String firstName;
		private String lastName;
		private String contact;
		private String email;
		private String password;
		
		public SignUpBeanClassBuilder setUserName(String userName) {
			this.userName = userName;
			return this;
		}
		public SignUpBeanClassBuilder setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}
		public SignUpBeanClassBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}
		public SignUpBeanClassBuilder setContact(String contact) {
			this.contact = contact;
			return this;
		}
		public SignUpBeanClassBuilder setEmail(String email) {
			this.email = email;
			return this;
		}
		public SignUpBeanClassBuilder setPassword(String password) {
			this.password = password;
			return this;
		}
		
		public SignUpBeanClass build() {
			return new SignUpBeanClass(this);
			
		}
		
	}
	
	public String getUserName() {
		return userName;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getContact() {
		return contact;
	}
	public String getEmail() {
		return email;
	}
	public String getPassword() {
		return password;
	}
	
	private SignUpBeanClass(SignUpBeanClassBuilder builder) {
		this.userName = builder.userName;
		this.firstName = builder.firstName;
		this.lastName =  builder.lastName;
		this.contact = builder.contact;
		this.email = builder.email;
		this.password = builder.password;
	}
}
