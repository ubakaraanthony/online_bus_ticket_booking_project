package SeatBooking;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.Cookie;

import ActionBean.PaymentOptions;
import ActionBean.SingleBusBean;
import ActionBean.UserBeanClass;
import Config.DBConfiguration;

public class SeatBookingServiceImpl extends DBConfiguration implements SeatBookingService {

	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	@Override
	public boolean bookSeats(String seats, String busId,String userName) {
		try {
			System.out.println("Bus ID"+busId);
			Class.forName(driver_class);
			con = DriverManager.getConnection(db_url, user, pass);
			System.out.println("DB Connected");
			ps = con.prepareStatement("select max(id) from booked_seats");
			rs = ps.executeQuery();
			int row = 0;
			int num=0;
			if(rs.next()) {
				row = rs.getInt(1);
			}
			System.out.println("count: "+row+1);
			System.out.println("userName: "+userName);
			System.out.println("BusID: "+busId);
			System.out.println("Seats: "+seats);

			ps = con.prepareStatement("insert into booked_seats "
					+ "(id,user_name,bus_id,booked_seat_no) values(?,?,?,?)");
			ps.setInt(1, row+1);
			ps.setString(2, userName);
			ps.setString(3, busId);
			ps.setString(4, seats);

			num = ps.executeUpdate();
			System.out.println("num"+num);
			if(num==1) {
				ps = con.prepareStatement("SELECT available_seats FROM bus_details WHERE bus_id=?");
				ps.setString(1, busId);
				rs = ps.executeQuery();
				if(rs.next()) {
					int numOfSeats = Integer.parseInt(rs.getString("available_seats"));
					int n = seats.split(",").length;
					System.out.println("Number of booked Seats: "+n);
					int availSeats = numOfSeats - n; 
					System.out.println(availSeats);
					ps = con.prepareStatement("update bus_details set available_seats = ? where bus_id= ?");
					ps.setString(1, String.valueOf(availSeats));
					ps.setString(2,busId);
					int number = ps.executeUpdate();
					System.out.println("Number: "+number);
					if(number==1) {
						System.out.println("available seats updated!!!");
					}
				}
				return true;
			}
		}	
		catch(Exception e) {
			e.printStackTrace();
		}
		return false;

	}

	@Override
	public boolean payOption(PaymentOptions paymentOption) {
		try {
			Class.forName(driver_class);
			con = DriverManager.getConnection(db_url, user, pass);
			System.out.println("DB Connected");
			ps = con.prepareStatement("select max(pay_id) from payment");
			rs = ps.executeQuery();
			int row = 0;
			int num=0;
			if(rs.next()) {
				row = rs.getInt(1);
			}
			ps = con.prepareStatement("insert into payment "
					+ "(pay_id,name,pay_option,atm_num,cvv_num,paid_amount,bus_id,uname) values(?,?,?,?,?,?,?,?)");
			ps.setInt(1, row+1);
			ps.setString(2,	paymentOption.getName());
			ps.setString(3,	paymentOption.getPayOption());
			ps.setString(4,	paymentOption.getAtmNum());
			ps.setString(5,	paymentOption.getCvvNum());
			ps.setString(6,	paymentOption.getPaidAmount());
			ps.setString(7,	paymentOption.getBusId());
			ps.setString(8,	paymentOption.getUname());
			num = ps.executeUpdate();
			if(num==1) {
				return true;
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}

