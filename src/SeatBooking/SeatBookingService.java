package SeatBooking;

import ActionBean.PaymentOptions;

public interface SeatBookingService {
	public boolean bookSeats(String seats, String busId,String uname);
	public boolean payOption(PaymentOptions paymentOption);
}
