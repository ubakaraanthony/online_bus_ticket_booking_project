<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="co"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Select your Seats</title>
<style type="text/css">
.gridv input[type=text]{
	width: 180px;
	height: 30px;
	padding: 5px;
	border: 1px solid gray;
	border-radius: 5px;
}
.drop{
width: 190px;
	height: 40px;
	padding: 5px;
	border: 1px solid gray;
	border-radius: 5px;
}
</style>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\\css\\Menu_bar.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\\css\\SeatBooking.css">
<script
	src="${pageContext.request.contextPath}\\js\\jquery-3.6.0.min.js"></script>
</head>
<body>
	<div class="menu-container">
		<img src="${pageContext.request.contextPath}\\images\\bus_icon.png"
			alt="bus_icon" />
		<h2>Book Bus</h2>
		<header>
			<ul>
				<li><a href="#">Print/Sms</a></li>
				<li><a href="#">Cancel Ticket</a></li>
				<li><img
					src="${pageContext.request.contextPath}\\images\\user_icon3.png"
					id="userIcon" alt="user_img"></li>
			</ul>
		</header>
	</div>
	<c:view>
		<h:form id="det">
			<section id="show_seats">
			<h:outputLabel value="#{payment.paidAmount}"/>
				<h:panelGrid columns="4" cellpadding="20" cellspacing="20"
					width="90%"
					style="font-weight:bold;
				color:#448F0A;font-size:16px;" styleClass="gridv">
					<h:outputLabel value="Bus Name : " />
					<h:inputText value="#{busDet.busName}" disabled="true" />
					<h:outputLabel value="Enter Your Name : " />
					<h:inputText id="name" value="#{payment.name}" />
					<h:outputLabel value="From : " />
					<h:inputText value="#{busDet.from}" disabled="true" />
					<h:outputLabel value="Age : " />
					<h:inputText value="#{payment.age}" />
					<h:outputLabel value="To : " />
					<h:inputText value="#{busDet.to}" disabled="true" />
					<h:outputLabel value="Payment Options : " />
					<h:selectOneMenu value="#{payment.payOption}" styleClass="drop">
						<c:selectItem itemValue="credit" itemLabel="Credit Card" />
						<c:selectItem itemValue="debit" itemLabel="Debit Card" />
					</h:selectOneMenu>
					<h:outputLabel value="Amount : " />
					<h:inputText id="amount" value="#{busDet.amount}" disabled="true" />
					<h:outputLabel value="ATM Number : " />
					<h:inputText value="#{payment.atmNum}" />
					<h:outputLabel value="CVV Number : " />
					<h:inputText value="#{payment.cvvNum}" />
					<h:outputLabel value="Pay Amount : " />
					<h:inputText id="payAmount" value="#{payment.paidAmount}"
						disabled="true" />
				</h:panelGrid>
				<div id="seats">
					<div id="seats_select">
						<span id="Change_Color"></span> <span id="steering"><img
							src="${pageContext.request.contextPath}\\images\\steering2.png"
							alt=""></span> <span id="line"></span>

						<h:selectManyCheckbox styleClass="grid" value="#{seatArr.seats}">
							<c:selectItem itemLabel="A11" itemValue="A11"
								itemDisabled="#{seats.contains('A11')}" />
							<c:selectItem itemLabel="B11" itemValue="B11"
								itemDisabled="#{seats.contains('B11')}" />
							<c:selectItem itemLabel="C11" itemValue="C11"
								itemDisabled="#{seats.contains('C11')}" />
							<c:selectItem itemLabel="D11" itemValue="D11"
								itemDisabled="#{seats.contains('D11')}" />
							<c:selectItem itemLabel="A12" itemValue="A12"
								itemDisabled="#{seats.contains('A12')}" />
							<c:selectItem itemLabel="B12" itemValue="B12"
								itemDisabled="#{seats.contains('B12')}" />
							<c:selectItem itemLabel="C12" itemValue="C12"
								itemDisabled="#{seats.contains('C12')}" />
							<c:selectItem itemLabel="D12" itemValue="D12"
								itemDisabled="#{seats.contains('D12')}" />
							<c:selectItem itemLabel="A13" itemValue="A13"
								itemDisabled="#{seats.contains('A13')}" />
							<c:selectItem itemLabel="B13" itemValue="B13"
								itemDisabled="#{seats.contains('B13')}" />
							<c:selectItem itemLabel="C13" itemValue="C13"
								itemDisabled="#{seats.contains('C13')}" />
							<c:selectItem itemLabel="D13" itemValue="D14"
								itemDisabled="#{seats.contains('D14')}" />
						</h:selectManyCheckbox>

					</div>
					<div id="seats_confirm">
						<h:commandButton styleClass="confirm_booking"
							value="Confirm Booking"
							action="#{seatBooking.bookSeat(seatArr,payment)}" />
					</div>
					<h:outputLabel value="#{err}" />
					<!--<h:outputLabel value="#{seats}" />-->
				</div>
			</section>
		</h:form>
	</c:view>
</body>
<script src="${pageContext.request.contextPath}\\js\\SeatSelection.js"></script>
<script src="${pageContext.request.contextPath}\\js\\disableSeats.js"></script>
<script type="text/javascript">
document.querySelectorAll(".grid input[type='checkbox']").forEach(box => box.addEventListener('click',() =>
{ let amount = document.getElementById('det:amount').value;
let payAmount =  document.getElementById('det:payAmount').value;
if(box.checked){
	if(!isNaN(parseInt(payAmount))){
		document.getElementById('det:payAmount').value = parseInt(document.getElementById('det:payAmount').value) + parseInt(amount);
	}
	else{
		document.getElementById('det:payAmount').value = amount;
	}
}
else{
	document.getElementById('det:payAmount').value = parseInt(document.getElementById('det:payAmount').value) - parseInt(amount);
}
}));
</script>
</html>