<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}\\css\\Menu_bar.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}\\css\\LogIn.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}\\css\\SignUp.css">
<title>Book Bus - Log In</title>
</head>
<body
	style="background-image: url('${pageContext.request.contextPath}\\images\\background.jpg');">
	<div class="menu-container">
		<img src="${pageContext.request.contextPath}\\images\\bus_icon.png"
			alt="bus_icon">
		<h2>Book Bus</h2>
	</div>
	<main class="login">
		<section class="login_box">
			<form action="../loginMap"
				method="post">
				<div id="uname_part">
					<label for="uname" id="unamelbl">User Name: </label> <input
						type="text" name="uname" id="uname" autocomplete="off">
					${err}
				</div>
				<div id="pass_part">
					<label for="pass" id="passlbl">Password: </label> <input
						type="password" name="pass" id="pass">
					<p id="pass_error"></p>
				</div>
				<div id="btn_part">
					<button id="submit">Submit</button>
				</div>
			</form>
			<div id="signUp">
				<p>
					Haven't registered yet? <span id="signUp_link">Sign Up</span>
				</p>
			</div>
		</section>
		<section id="signup_page_whole">
			<div id="signup_page_part">
				<span id="close">+</span>
				<form action="../signUpMap" method="post">
					<input type="text" name="username" placeholder="User Name*"
						autocomplete="off" /> <input type="text" name="first_name"
						placeholder="First Name*" autocomplete="off" /> <input type="text"
						name="last_name" placeholder="Last Name*" autocomplete="off" /> <input
						type="text" name="contact" placeholder="Contact Number*"
						autocomplete="off" /> <input type="email" name="email"
						placeholder="Email" autocomplete="off" /> <input type="password"
						name="password" placeholder="Password*" /> <input type="password"
						name="confirm_password" placeholder="Confirm Password*" />
					<div id="buttons">
						<button id="register">Register</button>
						<button id="reset">Reset</button>
					</div>
				</form>
			</div>
		</section>
	</main>
</body>
<script src="${pageContext.request.contextPath}\\js\\Login&SignUp.js"></script>
</html>