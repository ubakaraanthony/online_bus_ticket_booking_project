<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Select your Seats</title>
<link rel="stylesheet" type="text/css" href="..\\css\\Menu_bar.css">
<link rel="stylesheet" type="text/css" href="..\\css\\Dummy.css">
<script
	src="${pageContext.request.contextPath}\\js\\jquery-3.6.0.min.js"></script>
</head>
<body>
	<div class="menu-container">
		<img src="..\\images\\bus_icon.png" alt="bus_icon" />
		<h2>Book Bus</h2>
		<header>
			<ul>
				<li><a href="#">Print/Sms</a></li>
				<li><a href="#">Cancel Ticket</a></li>
				<li><img src="..\\images\\user_icon3.png" id="userIcon"
					alt="user_img"></li>
			</ul>
		</header>
	</div>
	<form action="#">
		<section id="show_seats">
			<div id="seats">
				<div id="seats_select">
					<span id="Change_Color"></span> <span id="steering"><img
						src="..\\images\\steering2.png" alt=""></span> <span id="line"></span>
					<c:view>

								<h:selectManyCheckbox styleClass="grid">
										<c:selectItem itemLabel="A11" />
										<c:selectItem itemLabel="B11" />
										<c:selectItem itemLabel="C11" />
										<c:selectItem itemLabel="D11" />
										<c:selectItem itemLabel="A12" />
										<c:selectItem itemLabel="B12" />
										<c:selectItem itemLabel="C12" />
										<c:selectItem itemLabel="D12" />
										<c:selectItem itemLabel="A13" />
										<c:selectItem itemLabel="B13" />
										<c:selectItem itemLabel="C13" />
										<c:selectItem itemLabel="D13" />
								</h:selectManyCheckbox>
					</c:view>
				</div>
				<div id="seats_confirm">
					<button id="confirm_booking">Confirm Booking</button>
				</div>
			</div>
		</section>
	</form>
</body>
<script src="${pageContext.request.contextPath}\\js\\BusList.js"></script>
</html>