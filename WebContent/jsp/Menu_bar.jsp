<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="..\\css\\Menu_bar.css">
    <title>Welcome</title>
</head>
<body>
    <div class="menu-container">
        <img src="..\\images\\bus_icon.png" alt="bus_icon">
            <h2>Book Bus</h2>
    <header>
        <ul>
            <li><a href="#">Print/Sms</a></li>
            <li><a href="#">Cancel Ticket</a></li>
            <li><img src="..\\images\\user_icon3.png" id="userIcon" alt="user_img"></li>
        </ul>
    </header>
</div>
<main class="main_content">
<div class="navigation">
    <nav>
        <ul>
            <li class="nav_items"><a href="#">Log In/Sign Up</a></li>
            <li class="nav_items"><a href="#">My Profile</a></li>
            <li class="nav_items"><a href="#">Change Password</a></li>
            <li class="nav_items"><a href="faces/jsp/MyTrips.xhtml">My Trips</a></li>
            <li class="nav_items"><a href="#">Sign out</a></li>
        </ul>
    </nav>
</div>
<section id="content">
<section class="bus_search" style="background:linear-gradient( rgba(248, 66, 105, 0.438) 100%, rgba(0, 0, 0, 0.541)100%),url('..\\images\\background.jpg');">
    <form action="${pageContext.request.contextPath}/SearchBusServlet">
    <div class="text_boxes">
    <div class="from_box">
        <label for="from" id="fromlbl">From</label>
        <input type="text" id="from" name="from"/>
    </div>
    <div class="to_box">
        <label for="to" id="tolbl">To</label>
        <input type="text" id="to" name="to"/>
    </div>
    <div class="date_box">
        <label for="date" id="datelbl">Pickup Date</label>
        <input type="date" id="date" name="pickUpDate"/>
    </div>
    </div>
    <div id="bus_search_div">
    <button id="bus_Search"><span id="text">Search Buses</span></button>
    </div>
</form>
</section>
<section id="bus_marketing">
<div class="item item1">
    <img src="..\\images\\safety1.png" alt="">
    <h3>SAFETY +</h3>
    <p>With Safety+ we have brought in a set of measures like Sanitized buses, mandatory masks etc. to ensure you travel safely.</p>
</div>
<div class="item item2">
    <img src="..\\images\\low_price.png" alt="">
    <h3>LOWEST PRICES</h3>
    <p>We always give you the lowest price with the best partner offers.</p>
</div>
<div class="item item3">
    <img src="..\\images\\benefits_icon.png" alt="">
    <h3>LOWEST PRICES</h3>
    <p>We always give you the lowest price with the best partner offers.</p>
</div>
</section>
</section>
</main>
<footer id="footer">
<div>
    <img src="..\\images\\bus_icon.png" alt="bus_icon">
    <p>
        RedBus is the world's largest online bus ticket booking service trusted by over 8 million happy customers globally. redBus offers bus ticket booking through its website,iOS and Android mobile apps for all major routes in India.
    </p>
</div>
</footer>
</body>
</html>