<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\\css\\Menu_bar.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}\\css\\Bus_list.css">
<title>Bus List</title>
</head>
<body>
	<div class="menu-container">
		<img src="${pageContext.request.contextPath}\\images\\bus_icon.png"
			alt="bus_icon">
		<h2>Book Bus</h2>
		<header>
			<ul>
				<li><a href="#">Print/Sms</a></li>
				<li><a href="#">Cancel Ticket</a></li>
				<li><img
					src="${pageContext.request.contextPath}\\images\\user_icon3.png"
					id="userIcon" alt="user_img"></li>
			</ul>
		</header>
	</div>
	<main class="main_content">
		<div class="navigation">
			<nav>
				<ul>
					<li class="nav_items"><a href="#">Log In/Sign Up</a></li>
					<li class="nav_items"><a href="#">My Profile</a></li>
					<li class="nav_items"><a href="#">Change Password</a></li>
					<li class="nav_items"><a href="#">My Trips</a></li>
					<li class="nav_items"><a href="#">Sign out</a></li>
				</ul>
			</nav>
		</div>
		<section id="bus_content">
			<section id="search">
				<a href="Menu_bar.html" id="back">Back</a> <input type="text"
					name="from" id="from" autocomplete="off" value="${from}" disabled="disabled">
				<input type="text" name="to" id="to" autocomplete="off" value="${to}"
					disabled="disabled">
				<button id="modify_search">Modify</button>
			</section>
			<section id="bus_list">
				<section id="toptable">
					<p>Name/Type</p>
					<p>From/To</p>
					<p>Start Time</p>
					<p>Departure Place</p>
					<p>Price</p>
					<p>Available Seats</p>
				</section>
				<c:forEach items="${busList}" var="buss">
					<div id=1 class="bus_items">
						<div id="bus_info">
							<p>${buss.busName}</p>
							<p>${buss.depaturePlace}</p>
							<p>${buss.startTime}</p>
							<p>${buss.arrivalPlace}</p>
							<p>&#8377;${buss.amount}</p>
							<p>${buss.availableSeats}</p>
							<p>${buss.busType}</p>
							<p>${buss.to}</p>
						</div>
						<div id="view_seat_div">
							<a href="${pageContext.request.contextPath}/ShowSeatServlet?busId=${buss.id}" 
							id="view_seats" style="text-align:center;line-height:2; color:black; 
							text-decoration:none;display: block;">View Seats</a>
						</div>
					</div>
				</c:forEach>
			</section>
		</section>
	</main>
</body>
</html>